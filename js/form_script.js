//<!-- Form script -->
//<script> 
$(document).ready(function(){
    // Initialize fullPage
    $('#fullpage').fullpage({
        menu: '#menu',
        fixedElements: '.navbar-fixed',
        navigation: true,
        navigationTooltips: ['What is?', 'Features', 'Our Cities', 'Partners', 'Join us!'],
        anchors: ['what-is', 'features', 'our-cities', 'partners', 'joinus'],
    });

    if("#partner") {
        $('.footer-partner').show();
    }

    $('ul.tabs').tabs();
    $('.scrollspy').scrollSpy();
});
            
    $('#submit-email').click(function(){
        var name = $('#fullname').val();
        var city = $('#cityname').val();
        var email = $('#email').val();
        var notes = $('#notes').val();
        var subject = $('#subject').text();
        //console.log(subject,"cek");
        $.getJSON('http://services.qlue.id/smartnation/smartnation_mail.php?subject='+subject+'&fullname='+name+'&cityname='+city+'&email='+email+'&notes='+notes,function(data){
            if (data.success== "true"){
                Materialize.toast('Thank you. Please check your email for our next updates!', 4000);
                $('#fullname').val("");
                $('#city').val("");
                $('#email').val("");
                $('#notes').val("");
            }
            else 
            {
                 Materialize.toast('Sorry, you already subscribed!', 4000);
            }   

        })
    })   
            
    $('#submit-email-2').click(function(){
        var name = $('#fullname-2').val();
        var city = $('#cityname-2').val();
        var email = $('#email-2').val();
        var notes = $('#notes-2').val();
        var subject = $('#subject-2').text();

        $.getJSON('http://services.qlue.id/smartnation/smartnation_mail.php?subject='+subject+'&fullname='+name+'&cityname='+city+'&email='+email+'&notes='+notes,function(data){
            if (data.success== "true"){
                Materialize.toast('Thank you. Please check your email for our next updates!', 4000);
                $('#fullname-2').val("");
                $('#city-2').val("");
                $('#email-2').val("");
                $('#notes-2').val("");
            }
            else
            {
                Materialize.toast('Sorry, you already subscribed!', 4000);
            }   

        })
    })            
        //</script>
    //<!-- Scripts  END -->